# Automating Installation of Nginx on EC2 Instances
=================================================


A recent task at work prompted me to automate the installation of nginx on EC2 instances. This process needs to be automated to reduce man power overhead, but most of all, the need for auto scaling groups to provision fresh EC2 instances with nginx installed and configured on the fly.

# Solution
--------

*   1x Security Group
*   1x EC2 Launch template
*   1x AWS Autoscaling Group Target
*   1x AWS Autoscaling Group
*   1x S3 bucket
*   1x SSL Certificate for your domain name.

Steps
-----

The strategy to getting this solution to work is to setup the instrumental components of the solution, then assemble them together.

### Create Security Groups

1.  Log into AWS EC2 console and search for EC2 service.
2.  On the left menu panel, scroll down to “Network and Security” section and select “Security Groups”.
3.  Create a new security group; Provide a descriptive name such as “http-only access”.
4.  Select the inbound tab and add a 2 rules using the HTTP preset.
5.  Set the first rule to source 0.0.0.0/0 for IPv4 addresses.
6.  Set the second rule to source ::/0 for IPv6 addresses.
7.  Save the settings and move select “outbound” tab.
8.  Add the same rules as step 5 and 6 and save.
9.  Go back to inbound tab and add another rule. Take note of the security group Id (ie: sg-06284b5e0dc13bef5).
10.  Add another http preset rule but set the source to the security group Id.
11.  Repeat step 10 in outbound tab with the same security group Id.
12.  Repeat steps 1-11, replacing “HTTP” with “HTTPS”.

Customize Nginx configuration file
----------------------------------

1.  [Download nginx.conf file](./nginx.conf).
2.  Open it with a text editor (ie: Visual Studio Code, notepad)
3.  Search for and modify the following line:

    rewrite ^/$ https://example.com redirect;

4\. Change the text between ^ and $ symbols with the path which you wish redirect to occur. If you use the example above with just a single forward slash, this would default to an immediate redirect when visiting your domain name (ie: example.com/).

Here’s a live example:

    rewrite ^/my-special-page.html$ https://example.com redirect;

The example above will trigger a redirect on [https://<your](https://<your) domain name>/my-special-page.html to [https://example.com](https://example.com).

5\. Change “example.com” to the url which you wish to forward your users to.

6\. Change the word “redirect” to “permanent” if you wish to signal a permanent redirect. For temporary redirect, leave it at “redirect”.

Create S3 Bucket
----------------

1.  From the services search bar at the top in AWS Console, search for S3.
2.  Create an S3 bucket for this type of automation (example: example-automation-bucket)
3.  Copy nginx.conf into the s3 bucket.
4.  Take note of the s3 path of where nginx.conf file is stored.

Create IAM Role and Policy for EC2 Instance
-------------------------------------------

1.  From the services search bar at the top in AWS Console, search for IAM.
2.  From the left menu bar, expand “Access management” section and click on “Policies”.
3.  Click on “Create policy” button.
4.  Copy and paste the following policy. Replace the text wrapped around with angle brackets with the actual values.

    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AllowS3BucketAccess",
                "Effect": "Allow",
                "Action": [
                    "s3:ListBucket",
                    "s3:GetObject"
                ],
                "Resource": [
                    "arn:aws:s3:::<my-s3-bucket>",
                    "arn:aws:s3:::<my-s3-bucket>/*"
                ]
            },
            {
                "Sid": "ListAllMyS3Buckets",
                "Effect": "Allow",
                "Action": "s3:ListAllMyBuckets",
                "Resource": "*"
            }
        ]
    }

5\. Save the policy and provide a name for the policy (ie: ec2-nginx-redirector-s3-access).

5\. From the left menu bar, expand “Access management” section and click on “Roles”.

6\. Click on “Create role” button.

7\. For trusted entity type select “AWS Service” and choose “EC2” as the service.

8\. When selecting permission policies, search for the policy created in steps 4 and 5.

9\. Provide a name ofr the role (ie: ec2-nginx-redirector) and save.

Create Launch Template
----------------------

1.  From the services search bar at the top in AWS Console, search for EC2.
2.  On the left menu panel scroll down “instances” section and select “Launch Templates”.
3.  Select “Create Launch Template” orange button.
4.  Provide a name for the launch template.
5.  For AMI, choose “Amazon Linux 2 HVM SSD (x86)”.
6.  In the Instance Type selection section, choose one of t2.micro, t3.micro, or t4g.micro**\*\***.

\*\*: t4g.micro is a new instance type being promoted by AWS at the time of writing. The promotion is for 750 hours of free usage until June 30th, 2021 (00:00 UTC time). Use of this instance will incur charges outside of the 750 hours or if the promotion has expired, whichever comes first.

7\. In the “Network settings” section, click on “security groups” dropdown menu and select the security group you’ve created above (ie: http-only-access).

8\. Expand the “Advance Details” section.

9\. Under “IAM instance profile”, select the IAM role you’ve created above (ie: ec2-nginx-redirector).

10\. Scroll down to “userdata” textfield.

11\. Copy and paste the following code:
```
    #! /bin/bash
    sudo amazon-linux-extras enable nginx1
    sudo yum clean metadata && sudo yum install -y nginx1
    aws s3 cp s3://<bucket name>/nginx.conf /etc/nginx/nginx.conf
    sudo systemctl enable nginx
    sudo systemctl start nginx
```
12\. Change “<bucket name>” to the actual name of the bucket you’ve created to store nginx.conf.

13\. Save the launch template.

Create Auto Scaling Group
-------------------------

1.  Still within AWS Console EC2 service, select “Auto Scaling Groups” under “Auto Scaling” section.
2.  Click “Create Auto Scaling group”.
3.  Provide a name for the Auto Scaling Group (ie: nginx-redirectors).
4.  In the “Launch Template” section, select the launch template you’ve created above and click “next”.
5.  Under “Network” section, select the appropriate VPC (or use the default provided VPC if you have not configured any).
6.  Select all subnets from the subnets drop down box.
7.  In the advanced options option page, leave everything to their default values and click “next”.
8.  In the “configure group size and scaling policies” page, set the desired, minimum, and maximum capacity. I would recommend maximum capacity to be at least 2 or more; Hit next once you’re done.
9.  Click “Next” in the “Add notifications page”.
10.  Add any tags you desire and click next; This is an optional step.

Create Auto Scaling target Group
--------------------------------

1.  Click into the name of the new auto scaling group you’ve just created.
2.  Click on “Actions” button and select “Launch instance from template”.
3.  From the left menu bar within AWS Console EC2 service, select “Target Groups”.
4.  Select “Create Target Groups”.
5.  Provide a target group name.
6.  Ensure protocol selected is “HTTP” and port number is “80”.
7.  Select the VPC used for Auto Scaling Group.
8.  Leave Protocol version in HTTP1.
9.  For health checks, leave protocol in HTTP.
10.  Set the health check path to the redirect path you’ve specified earlier in nginx.conf.
11.  Add optional tags.
12.  Click Next.
13.  Under available instances, check off the instances that were provisioned by the Auto Scaling Group.
14.  Click on “Include as pending below” button and hit “Create target group” button.

Create Application Load Balancer
--------------------------------

1.  Still within AWS Console EC2 service, select “Load Balancers” under “Load Balancing” section.
2.  Click on “Create Load Balancer” button.
3.  Click on the “create” button under Application Load Balancer.
4.  Provide a name for the balancer (ie: nginx-redirector).
5.  Under listeners, select “HTTPS” as the listener protocol.
6.  Under “Availability Zones”, select the VPC and subnets that was selected in the Auto Scaling Group.
7.  Click “Next” to configure security settings.
8.  For certificate type, select “Upload a certificate to ACM”.
9.  In the private key, paste the private key you’ve recieved from your DNS vendor.
10.  In the certificate body, paste in the server certificate.
11.  In the certificate chain, paste in the bottom certificate block followed by the top certificate block. This will allow the certificate to conform with the format specified by AWS Amazon Certificate Manager (ACM).
12.  For security policy, select “ELBSecurityPolicy-FS-1-2-Res-2020-10; This is the latest version at the time writing.
13.  Click “Next” to configure security groups.
14.  Select the HTTPs security group you’ve configured above and click “next”.
15.  Configure routing by selecting “Existing target group” from the “target group” dropdown menu and point it to the new target group you’ve created above.
16.  Review and deploy the load balancer; It may take a few minutes to provision the balancer.
17.  Take note of the CNAME of the

At this point, once the load balancer has been provisioned, a DNS name will be created for the load balancer which you can visit via the browser to have the request redirected to the EC2 instances provisioned by the auto scaling group with redirect.

Create Route 53 Entry
---------------------

1.  Create a hosted Zone for your website.
2.  Create an Alias record pointing to the CNAME of the load balancer above.

Bash Shell Script Explanation
-----------------------------

1.  Enables the nginx package from amazon-linux-extras package manager designed by Amazon.
2.  Clear metadata cache and install nginx.
3.  Copy the nginx configuration file from your S3 bucket to nginx’s configuration folder.
4.  Enable Nginx service such that it will auto start the next start the instance boots up.
5.  Start Nginx service.

Reference
---------

*   [How do I install a software package from the Extras Library on an EC2 instance running Amazon Linux 2?](https://aws.amazon.com/premiumsupport/knowledge-center/ec2-install-extras-library-software/)

